// [SECTION] Dependencies and Modules
const express = require('express');
const controller = require('../controllers/courses.js');
const auth = require('../auth')

// [SECTION] Routing Component
const route = express.Router();

// [SECTION] Route
	// Create Course [ADMIN]
		route.post('/create', auth.verify, (req, res) => {
			let isAdmin = auth.decode(req.headers.authorization).isAdmin
			let body = {
				course: req.body,
			}
			if (isAdmin) {
				controller.createCourse(body).then(result => res.send(result));
			}
			 else {
			 	res.send('User Unathorized to proceed')
			}	
		})

	// Retrieve All Course [ADMIN]
		route.get('/courses', auth.verify, (req, res) => {
			let token = req.headers.authorization;
			let isAdmin = auth.decode(token).isAdmin

			isAdmin ? controller.getAllCourses().then(result => res.send(result)) : res.send('Unathorized User')	
		})

	// Retrieve ONLY Active Course [ADMIN]
		route.get('/', (req, res) => {
			controller.getAllActive().then(result => res.send(result));
		})

	// Retrieve Single Course
		route.get('/:id', (req, res) => {
			let id = req.params.id
			controller.getCourse(id).then(result => res.send(result));
		})
 
	// Delete Course [ADMIN]
		route.delete('/:id', auth.verify, (req, res) => {
			let token = req.headers.authorization;
			let isAdmin = auth.decode(token).isAdmin;
			let id = req.params.id;

			isAdmin ? controller.deleteCourse(id).then(result => res.send(result)) : res.send('Unathorized User');
		})

	// Archive Course [ADMIN]
		route.put('/:id/archive', auth.verify, (req, res) => {
			let isAdmin = auth.decode(req.headers.authorization).isAdmin;
			let id = req.params.id;

			isAdmin ? controller.archiveCourse(id).then(result => res.send(result)) : res.send('Unathorized User')		
		})

	// Update Course [ADMIN]
		route.put('/:id/update', auth.verify, (req, res) => {
			let isAdmin = auth.decode(req.headers.authorization).isAdmin
			let id = req.params.id;
			let body = req.body;

			if (isAdmin) {
				controller.updateCourse(id, body).then(result => res.send(result));
			} 
			else {
				res.send('Unathorized user')
			}
			
		})

// [SECTION] Expose route
module.exports = route;
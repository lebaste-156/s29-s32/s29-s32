// [SECTION] Dependencies and Modules
	const express = require('express');
	const controller = require('../controllers/users');
	const auth = require('../auth');

// [SECTION] Route Component
	const route = express.Router();

// [SECTION] Route
	// Create User
	route.post('/create', (req, res) => {
		let body = req.body;
		controller.createUser(body).then(result => res.send(result));
	})

	// Login User
	route.post('/login', (req, res) => {
		let body = req.body;
		controller.userLogin(body).then(result => res.send(result));
	})

	// Authenticated User Course Enrollment
	route.post('/enroll', auth.verify, (req,res) => {
		let courseId = req.body.courseId;
		let token = req.headers.authorization;
		let userData = auth.decode(token);
		let userId = userData.id;
		let isAdmin = userData.isAdmin;

		let data = {
			userId: userId,
			courseId: courseId
		}
		
		isAdmin ? res.send('You cant enroll in a course') : controller.enroll(data).then(result => res.send(result))
	})

	// Check email exist
	route.post('/check-email', (req, res) => {
		controller.checkEmailExist(req.body).then(result => res.send(result));
	})

	// Retrieve All Users
	route.get('/users', (req, res) => {
		controller.getAllUsers().then(result => res.send(result));
	})

	// Set as Admin [ADMIN]
	route.put('/:id/set-admin', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin;
		let userId = req.params.id;

		isAdmin ? controller.setAdmin(userId).then(result => res.send(result)) : res.send('Unathorized User')
	})

	// Set as NonAdmin [ADMIN]
	route.put('/:id/set-non-admin', auth.verify, (req, res) => {
		let token = req.headers.authorization;
		let isAdmin = auth.decode(token).isAdmin;
		let userId = req.params.id;

		isAdmin ? controller.setNonAdmin(userId).then(result => res.send(result)) : res.send('Unathorized User')
	})

	// Retrieve Single User
	route.get('/id', auth.verify, (req, res) => {
		let userData = auth.decode(req.headers.authorization)
		let id = userData.id;
		controller.getUser(id).then(result => res.send(result));
	})

	// Delete User
	route.delete('/:id', (req, res) => {
		let id = req.params.id;
		controller.deleteUser(id).then(result => res.send(result));
	})

	// Update User
	route.put('/:id', (req, res) => {
		let id = req.params.id;
		let body = req.body;
		controller.updateUser(id, body).then(result => res.send(result));
})

// [SECTION] Expose route
module.exports = route;
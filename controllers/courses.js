// [SESCTION] Dependencies and Modules
const Course = require('../models/Course.js')

// [SECTION] Functionalities
	// Create Course [ADMIN]
	module.exports.createCourse = (reqBody) => {
		let name = reqBody.course.name;
		let description = reqBody.course.description;
		let price = reqBody.course.price;

		let newCourse = new Course({
			name: name ,
			description: description,
			price: price 
		})

		return newCourse.save().then((courseCreated, error) => {
			if (courseCreated) {
				return courseCreated;
			} 
			else {
				return false;
			}
		})
	}

	// Retrieve All Course [ADMIN]
	module.exports.getAllCourses = () => {
		return Course.find({}).then(result => {
			return result
		})
	}

	// Retrieve Only Active Course 
	module.exports.getAllActive = () => {
		return Course.find({isActive: true}).then(result => {
			return result;
		})
	}

	// Retrive Single Course
	module.exports.getCourse = (courseId) => {
		return Course.findById(courseId).then(result => {
			return result;
		})
	}

	// Delete Course
	module.exports.deleteCourse = (courseId) => {
		return Course.findByIdAndRemove(courseId).then((userDeleted, error) => {
			if(userDeleted) {
				return userDeleted
			}
			else {
				return false;
			}
		})
	}

	// Archive Course
	module.exports.archiveCourse = (courseId) => {

		let updatedStatus = {
			isActive: false
		}
		return Course.findByIdAndUpdate(courseId, updatedStatus).then((statusUpdated, error) => {
			if (statusUpdated) {
				return statusUpdated
			} 
			else {
				return false
			}
		})
	}

	// Update Course
	module.exports.updateCourse = (courseId, update) => {
		let cName = update.name ;
		let cDescription = update.description ;		
		let cPrice = update.price ;

		let updatedCourse = {
			name: cName,
			description: cDescription,
			price: cPrice
		}

		return Course.findByIdAndUpdate(courseId, updatedCourse).then((courseUpdated, error) => {
			if (courseUpdated) {
				return courseUpdated
			} 
			else {
				return false
			}
		})
	}
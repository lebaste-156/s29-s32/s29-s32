// [SECTION] Dependencies and Modules
const bcrypt = require('bcrypt')
const User = require('../models/User');
const auth = require('../auth')
const Course = require('../models/Course')

// [SECTION] Functionalities
	// Create User
	module.exports.createUser = (reqBody) => {
		let fName = reqBody.firstName;
		let lName = reqBody.lastName;
		let email = reqBody.email;
		let password = reqBody.password;
		let mobileNo = reqBody.mobileNo;

		let newUser = new User({
			firstName: fName,
			lastName: lName,
			email: email,
			password: bcrypt.hashSync(password, 10),
			mobileNo: mobileNo
		})
		return newUser.save().then((userCreated, error) => {
			if (userCreated) {
				return userCreated
			} 
			else {
				return false
			}
		})
	}

	// Login User
	module.exports.userLogin = (data) => {
		let uEmail = data.email;
		let uPass = data.password;
		return User.findOne({email: uEmail}).then(result => {
			if (result === null) {
				return false;
			} 
			else {
				let passW = result.password;
				const isMatched = bcrypt.compareSync(uPass, passW);
				if (isMatched) {
					return {access: auth.createAccessToken(result)};
				} else {
					return false;
				}
			}
		})
	}

	// Authenticated User Course Enrollment
	module.exports.enroll = async (data) => {
		let userId = data.userId;
		let courseId = data.courseId;

		let isUserUpdated = await User.findById(userId).then(result => {

			const isCourseFound = result.enrollments.find(courseObj => courseObj.courseId == courseId);

			if(!isCourseFound) {
				result.enrollments.push({courseId: courseId});
				return result.save().then((save, error) => {
					if (save) {
						return true;
					} 
					else {
						return false;
					}
				})
			}
			else {
				return false;
			}		
		})

		let isCourseUpdated = await Course.findById(courseId).then(result => {

			const isUserFound = result.enrollees.find(userObj => userObj.userId === userId);

			if(!isUserFound) {
				result.enrollees.push({userId: userId});
				return result.save().then((save, error) => {
					if (save) {
						return true;
					} 
					else {
						return false;
					}
				})
			}
			else {
				return false
			}			
		})

		if (isUserUpdated && isCourseUpdated) {
			return true
		} 
		else {
			return false
		}
	}

	// Email Checker
	module.exports.checkEmailExist = (reqBody) => {
		return User.find({email: reqBody.email}).then(result => {
			if (result.length > 0) {
				return false
			} 
			else {
				return true
			}
		})
	}

	// Retrieve All Users
	module.exports.getAllUsers = () => {
		return User.find({}).then(result => {
			return result;
		})
	}

	// Set as Admin [ADMIN]
	module.exports.setAdmin = (userId) => {
		let update = {
			isAdmin: true
		}
		return User.findByIdAndUpdate(userId, update).then((userUpdated, error) => {
			if (userUpdated) {
				return userUpdated;
			} 
			else {
				return false
			}
		})
	}

	// Set as NonAdmin [ADMIN]
	module.exports.setNonAdmin = (userId) => {
		let update = {
			isAdmin: false
		}
		return User.findByIdAndUpdate(userId, update).then((userUpdated, error) => {
			if (userUpdated) {
				return userUpdated;
			} 
			else {
				return false
			}
		})
	}

	// Retrieve Single User
	module.exports.getUser = (userId) => {
		return User.findById(userId).then(result => {
			result.password = ""
			return result;
		})
	}

	// Delete User
	module.exports.deleteUser = (userId) => {
		return User.findByIdAndRemove(userId).then((userDeleted, error) => {
			if (userDeleted) {
				return true
			} 
			else {
				return false
			}
		})
	}

	// Update User
	module.exports.updateUser = (userId, update) => {
		let fName = update.firstName;
		let lName = update.lastName;
		let email = update.email;		
		let password = update.password;
		let mobileNo = update.mobileNo;


		return User.findById(userId).then((foundUser, error) => {
			if (foundUser) {
	 			foundUser.lastName = lName
				foundUser.email = email
				foundUser.password = bcrypt.hashSync(password, 10);
				foundUser.mobileNo = mobileNo
				return foundUser.save().then((userUpdated, error) => {
					if(userUpdated) {
						return userUpdated;
					}
					else {
						return false
					}
				})
	 		} 
	 		else {
	 			return false
	 		}
	 	})
	 }

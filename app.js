//[SECTION] Dependencies and Modules
	const express = require('express');
	const mongoose = require("mongoose");
	const dotenv = require("dotenv");
	const cors = require("cors");
	const courseRoutes = require('./routes/courses')
	const userRoutes = require('./routes/users')

//[SECTION] Environment Variables Setup
	dotenv.config();
	const port = process.env.PORT;
	const connection = process.env.MONGO_URL

//[SECTION] Server Setup
	const app = express();
	app.use(cors());
	app.use(express.urlencoded({extended: true}));

// [SECTION] Route
	app.use(express.json());
	app.use('/courses', courseRoutes);
	app.use('/users', userRoutes);

//[SECTION] Database Connect
	mongoose.connect(connection);

	let db = mongoose.connection;
	db.on('open', () => console.log(`Connected to MongoDB`))

//[SECTION] Server Responses
	app.get('/', (req, res) => res.send('Project Deployed Successfully'))
	app.listen(port, () => {
   		console.log(`API is now online on port ${port}`);
	});
